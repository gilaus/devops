# Infrastructure

Problem: *snowflake environments*.

## IaC

Terraform example:
```
resource "aws_instance" "my_aws_instance" {
  ami           = "ami-0f3a43fbf2d3899f7"
  subnet_id     = "subnet-09e4f88f541fe0936"
  instance_type = "t2.nano"
}
```

* unambiguous **domain specific language (DSL)**
* declarative
* reproducible environments
* version control
* dry run 
* static checks
* docs

* cloud prerequisite?

## Best Terraform Practices

* shared [modules](https://github.com/terraform-aws-modules)
* multiple [providers](https://www.terraform.io/docs/providers/index.html)
* separate config and code
* CI!

## Environments

* local
    * limited feasibility in microservice / IoT world
* dev - sandbox for developers
    * unit testing
    * few components only
    * team ownership
    * use components from other environments?
* **int** - integration
    * DoD
    * low on resources
        * NFR?
    * 9-5/5
    * shared horse
* testing
    * is it necessary?
    * performance / load tests
    * security penetration tests
    * disaster recovery exercise
    * high costs... unless...
    * **one button full deploy**
* **pre-prod** -  mirror of production environment
    * networking!
    * resources?
    * dogfooding / early access :-)
    * exposed only to friendly users
    * user demo / user acceptance
    * no support
    * no confidential data
* **prod** - exposed to all users
    * and to the Internet
        * security
    * confidential / sensitive data
    * 24/7 availability (99%)
    * plus support
* on-premise
    * cloud / IoT paranoia
    * legal reasons
    * extra costs 
    * feature rollout?
    * extra features?
    * [Azure On-Premise](https://azure.microsoft.com/en-us/resources/videos/microsoft-azure-stack-azure-services-on-premises/)
    * [AWS GovCloud](https://aws.amazon.com/govcloud-us/) 

## Release Train

**Test, deploy, push forward and repeat**.

![environments](img/environments.png)

# Environment branches

[GitFlow](https://nvie.com/posts/a-successful-git-branching-model/)
![GitFlow](img/gitflow.png)

* link branches to environments
    * new node on named branch initiates deploy
    * preferably merge node
    * environment variables
    * [Gitlab Flow](https://about.gitlab.com/blog/2014/09/29/gitlab-flow/)
    * branches can be pointers to previous states of master, e.g. [OneFlow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
    
![GitLab Flow](img/environment_branches.png)
![OneFlow](img/develop-hotfix-branch-merge-final.png)

* IaC with product repo or separate repo?
    * build once
    * CI / CD for one product / microservice may separated into two repos
 
```
variables:
  DEPLOY_TARGET:        "DEV"
  VERSION:              "1.47.3-5-c0e4d120"
```


## Deployment Strategies

* Big Bang
* Rolling Deploy
* [Canary Release](https://martinfowler.com/bliki/CanaryRelease.html)
* [Blue Green Deployment](https://martinfowler.com/bliki/BlueGreenDeployment.html)
![AWS_ECS_BG](img/Implementing-Blue-Green-with-ECS.png)
* **Rainbow Deploy**
![Rainbow DevOps](img/rainbow.png)

## [Feature Toogles](https://martinfowler.com/articles/feature-toggles.html)

![Feature Toogles](img/feature-toggles.png)

# Challenges on MS

* zero downtime deploy
* even for IaC change
* one environment (to rule them all)
* e2e testing