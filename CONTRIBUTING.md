# Modify Content

* create an Issue see [example](https://code.siemens.com/evosoft-hu/clean-code-syllabus/issues/1)
  * fill in `Title` and `Description`
  * hit `Submit issue`
  * memorize the *number* of the issue (e.g. `#2`)
* fork this repo as your personal repo
* `Merge Requests` than `New merge request`
  * as `Source branch` select *your* `master` e.g. `istvan.szombath/devops`
  * as `Target branch` select `evosoft-hu/devops` `master`
  * edit `Title`, instead of `Master` add `Resolve "<<Issue Title>>"`
  * edit `Description`, add `Closes #<<Issue Number>>` e.g. `Closes #2`
* work on *your* master
* check content after CD on *your* Pages (Project Settings/ Pages)
* once finished remove the `WIP: ` flag from the merge request so that reviewer is informed that the merge request is finished
